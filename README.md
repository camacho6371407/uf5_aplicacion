# POO. PRÁCTICA UF5
## APLICACIÓN
### Yostin Arbey Camacho Velásquez  
  
Para este proyecto se pedía una aplicación con interfáz gráfica (Java.Swing) que permitiera   
realizar la gestión de profesores y alumnos. Para ello, se diseñáron una serie de formularios  
que se ocupan de la consulta y modificación(alta, edición y eliminación) de los registros de  
los actores anteriormente vistos. Además de un último formulario que mostrara en una tabla   
los distintos alumnos a los que un profesor imparte clase.  
  
La intención de este docuento es presentar el diseñó y un poco del funcionamiento de la plicación.  
En caso de querer mirar de forma más detallada el proyecto, el archivo con el mismo se encuentra   
en los documentos del repositorio.  
  
## VISIÓN DE LOS FORMULARIOS

### Formulario por defecto
Es la primera imagen que sale al iniciar la aplicón.  
![defecto](formularioDefecto.png)  
  
### Formularios de alta
Aquellos que permitern ingresar al sistema a alumnos y profesores.  
![altaAlumno](formularioAltaAlumno.png)  
![altaProfesor](formularioAltaProfesor.png)  
## Formularios de modificación/consulta
Estos formularios permiten consultar, editar y eliminar a la persona que  
consultamos.  
![modAlumno](formularioModAlumno.png)  
![modProfesor](formularioModProfesor.png)  
> Como se ve, estos formularios no tienen botón para salir. La razón de esto  
es porque al oprimirse dos veces de forma consecutiva al botón "Aceptar"  
este hace la función para volver al formulario por defecto.  

### Formulario para consultar alumnos
Este es el formulario que muestra los alumnos a los que un profesor da clase.  

![consultaAlumno](formularioConsultaAlumnos.png)  
---
## ROBUSTEZA
### Alertas y avisos
Para evitar personas duplicadas y entradas erroneas de datos, el programa interrumpe  
con una ventana emerjente que le avisa cuando está haciendo algo mal.
* Cuando se intenta ingresar a dos personas con el mismo DNI.  
![errorAlta](alertaAlta.png)  
* Cuando no se le asigna un profesor a un estudiante o el profesor no existe   
![altaNoProfesor](alertaNoProfesor.png)
* Cuando no se ingresa un entero como CP  
![errorCP](alertaCP.png)  
* Cuando no se ingresa un entero como precio de matrícula  
![errorMatricula](alertaMatricula.png)  
### Colecciones

Dentro del programa se puede decir que existen tres Map y un Set. Un mapa
gurda a los profesores,  
otro a los alumnos y el último, que hay uno dentro 
de cada profesor, guarda a los alumno a los que el  
 mismo  da clase.
Todos estos mapas son instancias de un TreeMap. Que al funcinar enpare clave-valor  
me permitían consultar mejor la información en
su interior. Además de no permiter objetos duplicados.  

Por último está el HashSet. Este corresponde a los alumnos y guarda
las actividades escolares del mismo.  
Lo elegí porque no quería que
una actividad se encontrara dor veces pero no me importaba el orden
de  
inserción las mismas.